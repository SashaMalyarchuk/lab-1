const fs = require('fs');
const readline = require('readline');


const rl = readline.createInterface({
    output: process.stdout,
    input: process.stdin
});


var data = {
    type: '',
    value: '',
    data: '',
    name: ''
}


async function asker() {
    data.type = await Ask("type");
    data.value = await Ask("value");
    data.data = await Ask("data");
    data.name = await Ask("name");

    rl.close();
    writeToFile(JSON.stringify(data));
}

function Ask(q) {
    return new Promise((resolve, reject) => {
        rl.question(q, (data) => {
            if (data) {
                console.log(data, 'received');
                resolve(data);
            } else {
                reject(new Error('something went wrong'));
            }
        });
    });

}

function writeToFile(data) {
    date = Date.now();
    fs.writeFileSync(`./dist/${data}-info.json`, data);
}

asker();